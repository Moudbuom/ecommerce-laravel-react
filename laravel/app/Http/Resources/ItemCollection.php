<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->is("api/items/category/*")) {
            /* 
            * return items that includes category: $cat_id
            */
            $cats = explode("/", $request->path());
            $cat_id = end($cats);
            return [
                'data' => $this->collection->filter(function($item) use ($cat_id){
                    // Convert $item->category format from :
                    // "[1, 2]" to [1,2]
                    $search = array("[", "]");
                    $parsed = explode(',',str_replace($search, '', $item->category));
                    return in_array((int) $cat_id, $parsed);
                })
            ];
        }
        return [
            'data' => $this->collection->transform(function($item){
            return [
                'title' => $item->title,
                'totalPrice' => round(( 1 - ($item->discount/100)) * $item->price,2),
                'discount' => $item->discount,
                'imageUrl' => $item->imageUrl,
                'category' => $item->category,
                'label' => $item->label,
                'slug' => $item->slug,
            ];
            })
        ];
    }
}
