<?php

namespace App\Http\Controllers;

use App\Model\Item;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Resources\ItemResource;
use App\Http\Resources\ItemCollection;
use Illuminate\Support\MessageBag;

use Symfony\Component\HttpFoundation\Response;


class ItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index','show');
        $this->middleware('admin')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return new ItemCollection(ItemResource::collection(Item::paginate(10)));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        //
        $item = new Item;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->stock = $request->stock;
        $item->price = $request->price;
        $item->discount = $request->discount;
        $item->category = $request->category;
        $item->slug = $request->slug;
        $item->label = $request->label;

        $image_filename = $request->slug."_".$request->label.".jpg";
        $item->imageUrl = $image_filename;
        $item->save();

        $path = $request->file('image')->move(public_path("/media/"), $image_filename);

        return response([
            'data' => new ItemResource($item)
        ],Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}
