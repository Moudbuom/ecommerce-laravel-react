<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;

class AuthController extends Controller
{
    //
    public $successStatus = 200;

    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['key'] =  $user->createToken('EcommerceApp')-> accessToken; 
            return response()->json($success, $this-> successStatus); 
        } 
        else{ 
            return response()->json(['status'=>'Unauthorized'], 401); 
        } 
    }

    public function register(UserRequest $request) 
    { 
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $input['admin'] = $this->checkIfAdmin($input);
        //TODO: check if creation of User ended successfully or not
        $user = AuthController::createUser($input); 
        $success['key'] =  $user->createToken('EcommerceApp')-> accessToken; 
        $success['name'] =  $user->name;
        return response()->json($success, $this-> successStatus);
    }

    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
    private function checkIfAdmin($input) {
        return $input['email'] == 'admin@admin.admin';
    }
    private static function createUser($input) {
        $u = new User;
        $u->name = $input['name'];
        $u->email = $input['email'];
        $u->password = $input['password'];
        $u->admin = $input['admin'];
        $u->save();
        return $u;
    }
}
