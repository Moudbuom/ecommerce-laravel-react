<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric|gte:0',
            'stock' => 'required|integer|gte:0',
            'discount' => 'required|numeric|between:0,99.99',
            'category' => 'required',
            'slug' => 'required|unique:items',
            'label' => 'required'
            
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */

    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'description.required'  => 'A description is required',
            'price.required' => 'Please fill the price field',
            'price.numeric' => 'price must be numeric',
            'price.gte' => 'price must be greater than or equal zero',
            'stock.required' => 'Please fill the stock field',
            'stock.integer' => 'stock must be an integer',
            'stock.gte' => 'stock must be greater than or equal zero',
        ];
    }

    /** * Get the validator instance for the request. 
    * * @param $factory \Illuminate\Validation\Factory 
    * * @return \Illuminate\Validation\Validator 
    */ 
    // public function validator($factory) 
    // { 
    //     return $factory->make( $this->all(), $this->rules() ); 
    // }
}
