<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'email.required'  => 'An email is required',
            'email.email'  => 'An email should be proper email',
            'password.required'  => 'A password is required',
            'c_password.required'  => 'confirm password is required',
            'c_password.same' => 'Confirm password should match'
        ];
    }
}
