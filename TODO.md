## frontend_admin
### TODO: Create new product
### TODO: ProductList is list view.

## reactjs
### DONE: Apply redux on ProductList
### DONE: Login view
### DONE: registeration view
### DONE: Create standalone components in Layout.js
### DONE: standalone Shop menu item in layout.js with hover effect
### DONE: Create dispatch that check auth from localstorage onTryAutoSignup
### TODO: Add several products with variations to database.
### TODO: ProductDetail page
### TODO: Work on cart
### TODO: How to search 
### TODO: Categorization of products , reflected in browsing
#### DONE: Backend implementation
#### TODO: Frontend implementation
#### TODO: Combine Backend and Frontend
#### Description
- immitate organization of www.decathlon.com  
- Categories assigned as a text formatted as list of ids.
 - i.e. Categories > "[1, 5, 313]"
- Categories in items table are ids mapping to categories table.
- Categories table populates categories as text formatted
 - i.e. 313 > "[By Activity, Combat Sports, Boxing, Protective Gear]"
 - i.e. 2 > "[For Men, Bottoms, Pants]"
### TODO: create utils package
### TODO: View products like decathlon
### TODO: Implement filter close to decathlon
### DONE: put each action/reducer component into separate directory

## php
### DONE: Functioning User passport
### DONE: ItemRequest blocked floats for price and discount
### DONE: Functioning Admin extending User
### DONE: Admin add Products -> User views them
### DONE: How to upload photos
### DONE: Customizing error messages UserRequest@messages 
- app does not send error messages for erratic user registeration request form.