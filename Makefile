dev: docker-compose.yml
	docker-compose -f docker-compose.yml up -d
	make init

postpone:
ifeq ($(OS), Windows_NT)
	timeout 12
else
	sleep 12
endif

init:
	make postpone
	docker-compose exec mysql bash -c "mysql -uhomestead -psecret < /home/ecommerce_db_backup.sql"
	docker-compose exec php php ../artisan serve --host 0.0.0.0 --port 8080


migrate:
	docker-compose exec django python manage.py migrate
	docker-compose exec php php /var/www/artisan migrate
	docker-compose exec php php /var/www/artisan db:seed