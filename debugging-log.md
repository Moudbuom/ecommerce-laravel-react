# laravel / php
## FormRequest does not send the error specified in messages()
- solution found in : https://jamesmills.co.uk/2019/06/05/how-to-return-json-from-laravel-form-request-validation-errors/ 
- What I did is to send the request with accept header set to application/json.
## default boolean value in migration table BUG
- default value of boolean column persists despite filling the column with other value on creation of model.
- This happens when using the Model::create() function.
- To encounter this use `new` and save() instead.
- Or use model->refresh() after Model::create()
 - but this adds new I/O on database.
## at start 500 srver error
- .env file was not exisiting
## Passport with CORS
### AuthServiceProvider does not recognize Route::
- use Illuminate\Support\Facades\Route
### $user->createToken() 
- throws "Trying to get property 'client' of non-object
- php artisan passport:install --force
- use HasApiTokens was not typed inside class User
### POST http://127.0.0.1:8080/api/register 401 (Unauthorized)
- formdata was incorrect
## OrderListState not updated
### in order_list_bloc.dart :
- use map<Orderitem> instead of plain map
 - since dynamic types does not create new state properly
### in order_list_state.dart
 - use BuiltList<Orderitem> instead of plain BuiltList constructor.
  - For the same reason we use map<Orderitem>
