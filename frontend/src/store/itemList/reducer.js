import { updateObject } from "../utility";
import { FETCH_ITEM_LIST_START, 
  FETCH_ITEM_LIST_SUCCESS, 
  FETCH_ITEM_LIST_FAIL, 
 } from "./actionTypes";

const initialState = {
  data: [],
  error: null,
  loading: false
};

const fetchItemListStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const fetchItemListSuccess = (state, action) => {
  return updateObject(state, {
    data: action.data,
    error: null,
    loading: false
  });
};

const fetchItemListFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEM_LIST_START:
      return fetchItemListStart(state, action);
    case FETCH_ITEM_LIST_SUCCESS:
      return fetchItemListSuccess(state, action);
    case FETCH_ITEM_LIST_FAIL:
      return fetchItemListFail(state, action);
    default:
      return state;
  }
};

export default reducer;
