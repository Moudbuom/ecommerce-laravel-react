import { FETCH_ITEM_LIST_START, 
  FETCH_ITEM_LIST_SUCCESS, 
  FETCH_ITEM_LIST_FAIL, 
 } from "./actionTypes";
import { productListURL, 
} from "../../constants";



const fetchItemListStart = () => {
  return {
    type: FETCH_ITEM_LIST_START
  };
};

const fetchItemListSuccess = data => {
  return {
    type: FETCH_ITEM_LIST_SUCCESS,
    data
  };
};

const fetchItemListFail = error => {
  return {
    type: FETCH_ITEM_LIST_FAIL,
    error: error
  };
};


export const fetchItemList = () => {
  return dispatch => {
    dispatch(fetchItemListStart());
    fetch(productListURL)
    .then(res=>res.json())
      .then(data => {
        console.log(data);
        dispatch(fetchItemListSuccess(data.data))
      })
      .catch(err => {
        console.log("error can't go there");
        //TODO: how to represent err from fetch()
        console.log(err);
        dispatch(fetchItemListFail(err));
      });
  };
};