import React from "react";
import StarRater from "./StarRater";
import {
  Card,
  Label,
  Image
} from "semantic-ui-react";


const GridItem = function(props) {
  return (
    <Card key={props.key} style={{float: "left", marginRight: "1vw", marginTop: "1vw", width: "30%"}}>
    <Image src={process.env.PUBLIC_URL + '/media/'+props.imageUrl} wrapped ui={false} />
    <Card.Content>
      <Card.Header onClick={() =>
            this.props.history.push(`/products/${props.id}`)}>MyShop</Card.Header>
      <Card.Header onClick={() =>
            this.props.history.push(`/products/${props.id}`)}>{props.title}</Card.Header>
      <Card.Meta>
      <span className="cinema">{props.category}</span>
      </Card.Meta>
      <Card.Description>
        <StarRater rating={props.rating}></StarRater>
      </Card.Description>
      <Card.Meta>
      <span className="cinema">$5.0</span>
      </Card.Meta>
      <Card.Meta>
      <span className="cinema">Free Shipping over $25</span>
      </Card.Meta>
      <Card.Meta>
      <span className="cinema">Free Pickup</span>
      </Card.Meta>
      <Card.Description>
      {props.description}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
    {props.discount_price && (
            <Label
              color={
                props.label === "P"
                  ? "blue"
                  : props.label === "secondary"
                  ? "green"
                  : "olive"
              }
            >
              {props.label}
            </Label>
          )}
    </Card.Content>
    </Card>
  );
}

export default GridItem;
