
import React from "react";

const StarRater = function(props) {
  const rating = props.rating;

  return(
    <div>
      <span class={`fa fa-star ${rating>=1? "checked":""}`}></span>
      <span class={`fa fa-star ${rating>=2? "checked":""}`}></span>
      <span class={`fa fa-star ${rating>=3? "checked":""}`}></span>
      <span class={`fa fa-star ${rating>=4? "checked":""}`}></span>
      <span class={`fa fa-star ${rating>=5? "checked":""}`}></span>
    </div>
  );
}

export default StarRater;