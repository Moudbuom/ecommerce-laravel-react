import React from "react";
import {
  Dropdown,
} from "semantic-ui-react";

export default class CartHeaderItem extends React.Component  {
  render() {
    return (
      <Dropdown
      icon="cart"
      loading={false}
      text={`Cart`}
      pointing
      className="link item"
    >
      <Dropdown.Menu>
        {<Dropdown.Item>No items in your cart</Dropdown.Item>}
      </Dropdown.Menu>
    </Dropdown>
    );
  }
}