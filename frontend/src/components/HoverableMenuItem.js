import React from "react";
import {
  Menu,
} from "semantic-ui-react";

export default class HoverableMenuItem extends React.Component  {
  state = {
    cursorOnItem: false,
  }

  hovering = () => {
    this.setState({
      cursorOnItem: true
    })
    setTimeout(() => {
      if (this.state.cursorOnItem) {
        this.props.onMouseEnter();
      }
    }, 1500)
  };
  mouseLeft = () => {
    this.setState({
      cursorOnItem: false,
    })
  }
  render() {
    return (
      <Menu.Item 
            onMouseEnter={() => this.hovering() }
            onMouseLeave = {() => this.mouseLeft()} >{this.props.children} </Menu.Item>
    );
  }

}