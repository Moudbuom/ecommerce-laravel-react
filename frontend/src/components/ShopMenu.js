
import React from "react";
import '../styles/mystyle.css';


const ShopMenu = {
  ShopMenu: function(props) {
    return(
      <div className= {`ui uppercase menu noborder nomargin`} {...props}>
        <div className={`ui container`}>
          <div className={`ui pointing menu`}>
            {props.children}
          </div>
        </div>
      </div>
    );
  },
  Dropdown: function(props) {
    const title = props.children[0];
    const columns = props.children.slice(1,) ;

    return(
    <div className={`megadropdown`}>
      {title}
      <div className={`megamenu`}>
        <div className={`ui container`}>
          <div className={`ui four column relaxed equal height divided stackable grid`}>
            {columns}
          </div>
        </div>
      </div>
      <div className={`filler`}></div>
    </div>
    );
  },
  DropdownTitle: function(props) {
    return (
      <a className={`browse item`}> {props.children} </a>
    );
  },
  Column: function(props) {
    return (
    <div className={`column`}>
      {props.children}
    </div>
    );
  },
  Header: function(props) {
    return (<h4 className={`ui header`}>{props.children}</h4>);
  },
  ItemList: function(props) {
    return (<div className={`ui link list`}>{props.children}</div>);
  },
  Item: function(props) {
    return (<a className={`item`}>{props.children}</a>);
  }
};

export default ShopMenu;