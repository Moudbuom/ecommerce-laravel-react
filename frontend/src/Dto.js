class _Product {
  constructor(id, slug, title, label, category, discount_price, description, image, variations) {
    this.id = id
    this.slug = slug
    this.title = title
    this.label = label
    this.category = category
    this.discount_price = discount_price
    this.description = description
    this.image = image
    this.variations = variations
  }
}

class ProductDto {
  constructor(item) {
    this._product = new _Product(item.id, item.slug, item.name, item.label, item.category, item.price, item.details, item.imageUrl, item.variations);
    this.convert = function () {
      return (this._product);
    }
  }
  
}

class ProductListDto {

  constructor(lst) {
    this._lst = lst.map(item => {
      return (new _Product(item.id, "", item.name, item.label, item.category, item.price, item.details, item.imageUrl, null));
    });

    this.convert = function () {
      return (this._lst);
    }
  }
}

export {ProductListDto, ProductDto};