const apiURL = "/api";

export const endpoint = `${apiURL}`;

export const productListURL = `${endpoint}/items/`;
export const loginURL = `${endpoint}/login/`;
export const registrationURL = `${endpoint}/register/`;

