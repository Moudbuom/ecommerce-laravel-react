import React from "react";
import {
  Container,
  Menu,
  Dropdown,
} from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../store/auth";
import CartHeaderItem from '../components/CartHeaderItem';
import HoverableMenuItem from '../components/HoverableMenuItem';
import { styler } from '../styles/layout.js';
import LayoutMenu from './LayoutMenu';
import LayoutFooter from './LayoutFooter';


class CustomLayout extends React.Component {
  state = {
    showModal: false,
  }

  handleItemClick = () => console.log("handling");
  hovering = () => {
    this.setState({
      showModal: true
    });
  };
  mouseLeaveModal = () => {
    this.setState({
      showModal: false,
    })
  }
  render() {
    const { authenticated } = this.props;
    
    return (
      <div>
        <Menu style={styler.mn0}>
          <Container>
            <Link to="/">
              <Menu.Item header>Home</Menu.Item>
            </Link>

            <HoverableMenuItem onMouseEnter= {this.hovering}>
              Shop
            </HoverableMenuItem>


            {/*  */}
            

            <React.Fragment>
              <Dropdown item text='Shop'>
                <Dropdown.Menu>
                <React.Fragment>
        <React.Fragment>
          <Dropdown.Header>Products</Dropdown.Header>

            <Dropdown.Item
              name='enterprise'
              active={false}
              onClick={this.handleItemClick}
            >enterprise</Dropdown.Item>
            <Dropdown.Item
              name='consumer'
              active={true}
              onClick={this.handleItemClick}
            >consumer</Dropdown.Item>
          </React.Fragment>

        <Menu.Item>
          <Menu.Header>CMS Solutions</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name='rails'
              active={true}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name='python'
              active={false}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name='php'
              active={false}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu.Item>

        <Menu.Item>
          <Menu.Header>Hosting</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name='shared'
              active={false}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name='dedicated'
              active={false}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu.Item>

        <Menu.Item>
          <Menu.Header>Support</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name='email'
              active={true}
              onClick={this.handleItemClick}
            >
              E-mail Support
            </Menu.Item>

            <Menu.Item
              name='faq'
              active={true}
              onClick={this.handleItemClick}
            >
              FAQs
            </Menu.Item>
          </Menu.Menu>
        </Menu.Item>
        </React.Fragment>
      </Dropdown.Menu>
              </Dropdown>
            </React.Fragment>
            {/*  */}



            <Link to="/products">
              <Menu.Item header>Products</Menu.Item>
            </Link>

            {authenticated ? (
              <React.Fragment>
                <Menu.Menu position="right">
                  <Link to="/profile">
                    <Menu.Item>Profile</Menu.Item>
                  </Link>
                  <CartHeaderItem></CartHeaderItem>
                  <Menu.Item header onClick={() => this.props.logout()}>
                    Logout
                  </Menu.Item>
                </Menu.Menu>
              </React.Fragment>
            ):(<Menu.Menu position="right">
                <Link to="/login">
                  <Menu.Item header>Login</Menu.Item>
                </Link>
                <Link to="/signup">
                  <Menu.Item header>Signup</Menu.Item>
                </Link>
              </Menu.Menu>
              )}
            
          </Container>
        </Menu>

        {this.state.showModal? 
            <LayoutMenu onMouseLeave= {()=> this.mouseLeaveModal()}></LayoutMenu>
            :this.props.children}

          <LayoutFooter></LayoutFooter>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout()),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CustomLayout)
);