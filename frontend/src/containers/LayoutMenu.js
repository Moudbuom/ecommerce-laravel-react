import React from "react";

import ShopMenu from '../components/ShopMenu';

export default class LayoutMenu extends React.Component {
  render() {
    return (
      <ShopMenu.ShopMenu {...this.props}>
        <ShopMenu.Dropdown>
          <ShopMenu.DropdownTitle> For Women </ShopMenu.DropdownTitle>
          <ShopMenu.Column>
            <ShopMenu.Header>Fabrics</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Viscose</ShopMenu.Item>
            </ShopMenu.ItemList>
            <ShopMenu.Header>Fabrics Level 2</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Cashmere</ShopMenu.Item>
              <ShopMenu.Item>Linen</ShopMenu.Item>
              <ShopMenu.Item>Cotton</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Footwear</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Sandals</ShopMenu.Item>
              <ShopMenu.Item>Running Shoes</ShopMenu.Item>
              <ShopMenu.Item>Hiking Boots</ShopMenu.Item>
              <ShopMenu.Item>Water Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Tops</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Tanks</ShopMenu.Item>
              <ShopMenu.Item>Sweatshirts</ShopMenu.Item>
              <ShopMenu.Item>Hoodies</ShopMenu.Item>
              <ShopMenu.Item>Shirts</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Types</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Knitwear</ShopMenu.Item>
              <ShopMenu.Item>Outwear</ShopMenu.Item>
              <ShopMenu.Item>Pants</ShopMenu.Item>
              <ShopMenu.Item>Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>
        </ShopMenu.Dropdown>

        <ShopMenu.Dropdown>
          <ShopMenu.DropdownTitle> For Men </ShopMenu.DropdownTitle>
          <ShopMenu.Column>
            <ShopMenu.Header>Fabrics</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Viscose</ShopMenu.Item>
            </ShopMenu.ItemList>
            <ShopMenu.Header>Fabrics Level 2</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Cashmere</ShopMenu.Item>
              <ShopMenu.Item>Linen</ShopMenu.Item>
              <ShopMenu.Item>Cotton</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Footwear</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Sandals</ShopMenu.Item>
              <ShopMenu.Item>Running Shoes</ShopMenu.Item>
              <ShopMenu.Item>Hiking Boots</ShopMenu.Item>
              <ShopMenu.Item>Water Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Tops</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Tanks</ShopMenu.Item>
              <ShopMenu.Item>Sweatshirts</ShopMenu.Item>
              <ShopMenu.Item>Hoodies</ShopMenu.Item>
              <ShopMenu.Item>Shirts</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Types</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Knitwear</ShopMenu.Item>
              <ShopMenu.Item>Outwear</ShopMenu.Item>
              <ShopMenu.Item>Pants</ShopMenu.Item>
              <ShopMenu.Item>Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>
        </ShopMenu.Dropdown>

        <ShopMenu.Dropdown>
          <ShopMenu.DropdownTitle>For Kids</ShopMenu.DropdownTitle>
          <ShopMenu.Column>
            <ShopMenu.Header>Fabrics</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Viscose</ShopMenu.Item>
            </ShopMenu.ItemList>
            <ShopMenu.Header>Fabrics Level 2</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Cashmere</ShopMenu.Item>
              <ShopMenu.Item>Linen</ShopMenu.Item>
              <ShopMenu.Item>Cotton</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Footwear</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Sandals</ShopMenu.Item>
              <ShopMenu.Item>Running Shoes</ShopMenu.Item>
              <ShopMenu.Item>Hiking Boots</ShopMenu.Item>
              <ShopMenu.Item>Water Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Tops</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Tanks</ShopMenu.Item>
              <ShopMenu.Item>Sweatshirts</ShopMenu.Item>
              <ShopMenu.Item>Hoodies</ShopMenu.Item>
              <ShopMenu.Item>Shirts</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Types</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Knitwear</ShopMenu.Item>
              <ShopMenu.Item>Outwear</ShopMenu.Item>
              <ShopMenu.Item>Pants</ShopMenu.Item>
              <ShopMenu.Item>Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>
        </ShopMenu.Dropdown>

        <ShopMenu.Dropdown>
          <ShopMenu.DropdownTitle>Gear</ShopMenu.DropdownTitle>
          <ShopMenu.Column>
            <ShopMenu.Header>Fabrics</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Viscose</ShopMenu.Item>
            </ShopMenu.ItemList>
            <ShopMenu.Header>Fabrics Level 2</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Cashmere</ShopMenu.Item>
              <ShopMenu.Item>Linen</ShopMenu.Item>
              <ShopMenu.Item>Cotton</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Footwear</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Sandals</ShopMenu.Item>
              <ShopMenu.Item>Running Shoes</ShopMenu.Item>
              <ShopMenu.Item>Hiking Boots</ShopMenu.Item>
              <ShopMenu.Item>Water Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Tops</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Tanks</ShopMenu.Item>
              <ShopMenu.Item>Sweatshirts</ShopMenu.Item>
              <ShopMenu.Item>Hoodies</ShopMenu.Item>
              <ShopMenu.Item>Shirts</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>

          <ShopMenu.Column>
            <ShopMenu.Header>Types</ShopMenu.Header>
            <ShopMenu.ItemList>
              <ShopMenu.Item>Knitwear</ShopMenu.Item>
              <ShopMenu.Item>Outwear</ShopMenu.Item>
              <ShopMenu.Item>Pants</ShopMenu.Item>
              <ShopMenu.Item>Shoes</ShopMenu.Item>
            </ShopMenu.ItemList>
          </ShopMenu.Column>
        </ShopMenu.Dropdown>
      </ShopMenu.ShopMenu>
    );
  }
}