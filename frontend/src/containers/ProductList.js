import React from "react";
import '../styles/productlist.css';
import {
  Container,
  Dimmer,
  Image,
  Loader,
  Message,
  Segment
} from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { fetchItemList } from "../store/itemList";
import GridItem from "../components/GridItem";


class ProductList extends React.Component {
  state = {
    loading: false,
    error: null,
    data: []
  };

  componentDidMount() {
    this.props.fetchItemList();
  }

  render() {
    const { data, error, loading } = this.props;
    return (
      <Container style={{marginTop: "2vh"}}>
        <aside>
          <p>some Text</p>
        </aside>
        <section>
        {error && (
          <Message
            error
            header="There was some errors with your submission"
            content={JSON.stringify(error)}
          />
        )}
        {loading && (
          <Segment>
            <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>

            <Image src="/images/wireframe/short-paragraph.png" />
          </Segment>
        )}
          {data.map(item => {
            return (
              <GridItem 
              key={item.slug} 
              id={item.label}
              title={item.title}
              imageUrl={item.imageUrl}
              rating={4}
              category={item.category}
              description={item.description}
              discount_price={item.discount_price}
              label={item.label}
              ></GridItem>
              
            );
          })}
          <div className={`clear`}></div>
        </section>
        <div className={`clear`}></div>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.itemList.loading ,
    error: state.itemList.error,
    data: state.itemList.data,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchItemList: ()=>dispatch(fetchItemList()),
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductList)
);