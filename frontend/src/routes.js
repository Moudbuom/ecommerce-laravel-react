import React from "react";
import { Route } from "react-router-dom";
import Hoc from "./hoc/hoc";

import HomepageLayout from "./containers/Home";
import ProductList from "./containers/ProductList";
import LoginForm from "./containers/Login";
import Signup from "./containers/Signup";





const BaseRouter = () => (
  <Hoc>
    <Route exact path="/" component={HomepageLayout} />
    <Route exact path="/products" component={ProductList} />
    <Route path="/login" component={LoginForm} />
    <Route path="/signup" component={Signup} />
  </Hoc>
);

export default BaseRouter;
